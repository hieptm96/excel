class StaticPagesController < ApplicationController
	require "axlsx"
	def index
		respond_to do |format|
		    format.html
		    format.xlsx {
			    response.headers['Content-Disposition'] = 'attachment; filename="index.xlsx"'
			}
		end
	end
end
